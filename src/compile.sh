#!/bin/bash

#pdflatex cv.tex
#bibtex Papers.aux
#bibtex Conferences.aux
#bibtex Brevets.aux
#bibtex Workshops.aux
#pdflatex cv.tex
#pdflatex cv.tex
#mv cv.pdf cv-en.pdf
#
#pdflatex '\def\exportfr{1} \input{cv}'
#bibtex Papers.aux
#bibtex Conferences.aux
#bibtex Brevets.aux
#bibtex Workshops.aux
#pdflatex '\def\exportfr{1} \input{cv}'
#pdflatex '\def\exportfr{1} \input{cv}'
#mv cv.pdf cv-fr.pdf

pdflatex cv-fr.tex
for file in $(ls *.aux)
do
  echo "Preparing bibliography ${file}";
  bibtex ${file};
done
pdflatex cv-fr.tex

pdflatex cv-en.tex
for file in $(ls *.aux)
do
  echo "Preparing bibliography ${file}";
  bibtex ${file};
done
pdflatex cv-en.tex
